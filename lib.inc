section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ; Код системного вызова 
    syscall ; системный вызов

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rax + rdi], 0 ; Если конец - нуль-терминированная строка, переход в .end
        je .end ; переход если ZF == 1
        inc rax ; увеличение длинны строки на 1 
        jmp .loop ; переход в начало цикла 
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi;
    call string_length ; вызов функции string_length
    mov rdx, rax ; длинна строки 
    pop rsi; ; указатель на строку
    mov rdi, 1 ;  
    mov rax, 1 ; системный вызов "write"             
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    mov rax, 1
    mov rdx, 1
    push rdi
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret 


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi 
    mov rdi, rsp
    dec rdi
    mov r11, 10
    push 0
    sub rsp, 16
.loop:
    xor rdx, rdx
    div r11
    dec rdi
    add rdx, '0'
    mov byte[rdi], dl
    test rax, rax
    jnz .loop
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0 
    jge .positive
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.positive:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rsi
    call string_length
    pop rdi
    push rax
    call string_length
    pop rdx
    cmp rdx, rax
    jne .false
    mov rax, 1
    ret
.false:
    xor rax, rax
    ret
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

; rdi - адрес начала буфера 
; rsi - размер буфера
read_word:
    xor rcx, rcx
.loop:
    cmp rdi, 0
    jle .err
    push rcx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rcx
    cmp rax, 0x20
    je .whitespace    
    cmp rax, 0xA
    je .whitespace
    cmp rax, 0x9
    je .whitespace
    cmp rax, 0
    je .end 
    mov byte [rdi + rcx], al
    inc rcx
    cmp rcx, rsi
    jge .err
    jmp .loop
.whitespace:
    cmp rcx, 0
    je .loop
    jmp .end
.err:    
    xor rax, rax
    xor rdx, rdx 
    ret
.end: 
    xor rax, rax
    mov [rdi + rcx], rax
    mov rax, rdi
    mov rdx, rcx
    ret           


 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r9, 10 
    xor rax, rax
    xor rcx, rcx
.loop:
    movzx r8, byte[rdi + rcx]
    cmp r8b, 0
    je .end
    cmp r8b, '0'
    jb .end
    cmp r8b, '9'
    ja .end
    mul r9
    sub r8b, '0'
    add rax, r8
    inc rcx
    jmp .loop
.end:
    mov rdx, rcx    
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    je .negative 
.positive: 
    call parse_uint
    ret 
.negative:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jae .small_buff
    push rsi
.strcpy_loop: 
    mov dl, byte[rdi]
    mov byte[rsi], dl
    inc rdi
    inc rsi
    test dl, dl
    jnz .strcpy_loop
    pop rax
    ret
.small_buff:
    mov rax, 0
    ret


